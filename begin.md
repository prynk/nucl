>A journey of a thousand miles begins with a single step.
>
>- [Lao Tse](https://en.wiktionary.org/wiki/Laozi)


>Afoot and lighthearted I take to the open road, healthy, free, the world
>before me.
>
>- [Walt Whitman](https://www.goodreads.com/quotes/33266-afoot-and-lighthearted-i-take-to-the-open-road-healthy)


>Remember tonight... for it is the beginning of always.
>- [Dante Alighieri] (https://www.goodreads.com/quotes/136812-remember-tonight-for-it-is-the-beginning-of-always)
Not sure if Dante really wrote this, I love this perspective nonetheless.